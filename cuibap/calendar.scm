;; -*- indent-tabs-mode: nil -*-
(define-module cuibap.calendar
  (use srfi-1)
  (use srfi-19)
  (use srfi-42)
  (export calendar
          month-name
          weekday-name))
(select-module cuibap.calendar)

(define (split-when pred? list)
  (let loop ([item '()]
             [result '()]
             [list list])
    (cond
     [(null? list) (reverse (if (null? item)
                                result
                                (cons (reverse item) result)))]
     [(pred? list) (loop (cons (car list) '())
                         (if (null? item)
                             result
                             (cons (reverse item) result))
                         (cdr list))]
     (else (loop (cons (car list) item)
                 result
                 (cdr list))))))

(define (get-dates-in-a-month now)
  (let* ([first-day (make-date 1 1 1 1
                               1
                               (date-month now)
                               (date-year now)
                               (date-zone-offset now))]
         [same-month? (lambda (date)
                        (eqv? (date-month date)
                              (date-month now)))]
         [time-diff (lambda (nr-days)
                      (time-difference (make-time time-utc 0 (* nr-days 60 60 24))
                                       (make-time time-utc 0 0)))]
         [add-day (lambda (date nr-days)
                    (time-utc->date (add-duration (date->time-utc date)
                                                  (time-diff nr-days))
                                    (date-zone-offset date)))])
    (filter same-month?
            (list-ec (: nr-days 32)
                     (add-day first-day nr-days)))))

(define (pad-first-week weeks)
  (let ([padding (- 7 (length (first weeks)))])
    (if (> padding 0)
        (cons (append (list-ec (: i padding) #f) (first weeks))
              (cdr weeks))
        weeks)))

(define (calendar now)
  (let ([weeks (split-when (lambda (list)
                             (eqv? (date-week-day (car list)) 1))
                           (get-dates-in-a-month now))])

    (map (lambda (week)
           (map (lambda (date)
                  (and (date? date)
                       (date-day date)))
                week))
         (pad-first-week weeks))))

(define (weekday-name day)
  (list-ref '("CN" "T2" "T3" "T4" "T5" "T6" "T7") day))

(define (month-name month)
  (list-ref '("Tháng một" "Tháng hai" "Tháng ba" "Tháng tư"
              "Tháng năm" "Tháng sáu" "Tháng bảy" "Tháng tám"
              "Tháng chín" "Tháng mười"
              "Tháng mười một" "Tháng mười hai")
            (- month 1)))

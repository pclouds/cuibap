;; -*- indent-tabs-mode: nil -*-
(define-module cuibap.storage
  (use cuibap.nanoblogger)
  (use cuibap.params)
  (use file.util)
  (use gauche.process)
  (use srfi-1)
  (use srfi-19)
  (use srfi-13)
  (use srfi-28)
  (use srfi-42)
  (export add-new-post
          call-with-input-post
          commit->date
          commit-new-post
          delete-new-post
          get-last-id-per-day-alist
          get-months
          get-next-month
          get-prev-month
          get-years
          modified-posts
          new-post-path
          post-alist
          post-commits
          post-id?
          post-id->month
          post-id->path
          post-id->year
          post-list
          post-path->id
          posts-by-month
          posts-by-year
          recent-posts))
(select-module cuibap.storage)

(define *post-alist-cache* #f)          ; ( *git-dir* . (post-alist) )
(define *git-log-raw-cache* #f)

(define-syntax git-dir-cache-through!
  (syntax-rules ()
    [(_ cache proc) (begin
                      (unless (and (pair? cache)
                                   (string=? (car cache) (*git-dir*)))
                        (set! cache (cons (*git-dir*) (proc))))
                      (cdr cache))]))

(define (post-id? string)
  (#/^\d\d\d\d-\d\d-\d\dT\d\d_\d\d_\d\d$/ string))

(define (post-path? path)
  (#/(\d\d\d\d-\d\d-\d\dT\d\d_\d\d_\d\d)\.txt/ (sys-basename path)))

(define (post-path->id path)
  (if-let1 match (post-path? path)
    (match 1)
    #f))

(define (decompose-post-id id)
  (let ([m (#/(\d\d\d\d)-(\d\d)-(\d\d)T(\d\d)_(\d\d)_(\d\d)/ id)])
    (apply values (list-ec (:range x 1 7) (string->number (m x))))))

(define (post-id->year id)
  (receive (year month day hour minute second)
      (decompose-post-id id)
    year))

(define (post-id->month id)
  (receive (year month day hour minute second)
      (decompose-post-id id)
    month))

(define (post-id->day id)
  (receive (year month day hour minute second)
      (decompose-post-id id)
    day))

(define (post-id->path id)
  (let ([item (assoc id (post-alist))])
    (and item (cdr item))))

(define (%post-alist)
  (define (ls-files)
    (process-output->string-list
     (list "git" "-C" (*git-dir*) "ls-files" "--" "*.txt" ":!files/")))

  (define (files->alist files)
    (filter-map (lambda (path)
                  (if-let1 match (post-path? path)
                    (cons (match 1) path)
                    #f))
                files))

  (sort (files->alist (ls-files)) string-comparator car))

(define (post-alist)
  (git-dir-cache-through! *post-alist-cache* %post-alist))

(define (post-list)
  (map car (post-alist)))

(define (posts-by-year year)
  (filter (string->regexp (format #f "^~d-" year))
          (post-list)))

(define (posts-by-month month year)
  (filter (string->regexp (format #f "^~d-~2,'0d-" year month))
          (post-list)))

(define (recent-posts)
  (take-right* (post-list) (*max-entries*)))

(define (modified-posts :key head)
  (let ([files (process-output->string-list
                (list "git" "-C" (*git-dir*)
                      "diff"
                      "--name-only"
                      head
                      "--" "*.txt"))])
    (map post-path->id (filter post-path? files))))

(define (get-years)
  (delete-duplicates
   (map post-id->year (post-list))))

(define (get-months year)
  (delete-duplicates
   (map post-id->month (posts-by-year year))))

(define (get-last-id-per-day-alist posts)
  (fold-right (lambda (head tail)
                (let ([day (post-id->day head)])
                  (if (and (pair? tail)
                           (eqv? (caar tail) day))
                      tail
                      (cons (cons day head) tail))))
              '()
              posts))

(define (get-adjacent-month month year op)
  (let* ([months (get-months year)]
         [index (list-index (cut eqv? <> month) months)])
    (if (number? index)
        (let ([index (op index 1)])
          (if (and (<= 0 index)
                   (< index (length months)))
              (list-ref months index)
              #f))
        #f)))

(define (get-prev-month month year)
  (get-adjacent-month month year -))

(define (get-next-month month year)
  (get-adjacent-month month year +))

(define (call-with-input-post id proc)
  (call-with-input-file
      (build-path (*git-dir*) (post-id->path id))
    proc))

(define (group-lines pred? lines)
  (map (lambda (x)
         (cons (car x) (reverse (cdr x))))
       (reverse
        (fold (lambda (line rest)
                (if (pred? line)
                    (cons (list line) rest)
                    (let ([last-line (car rest)]
                          [rest (cdr rest)])
                      (cons (apply list (car last-line) line (cdr last-line))
                            rest))))
              '()
              lines))))

;; LINE -> (NAME EMAIL TIMESTAMP TIMEZONE)
(define (parse-ident-line ident)
  (let ([fields  (string-split ident
                               (^x (or (eqv? x #\<)
                                       (eqv? x #\>)))
                               2)])
    (let ([name (string-trim-both (first fields))]
          [email (second fields)]
          [timestamp (string-trim-both (third fields))])
      (list name
            email
            (map string->number
                 (string-split timestamp " " 1))))))

(define (parse-header line)
  (let ([fields (string-split line " " 1)])
    (let ([name (string->symbol (car fields))]
          [value (cadr fields)])
      (cons name
            (case name
              [(author committer) (parse-ident-line value)]
              [else value])))))

(define (parse-diff line)
  (let ([line (string-split line #\tab)])
    (if (post-path? (cadr line))
        (cons (post-path->id (cadr line))
              (string-tokenize (car line)))
        #f)))

(define (parse-raw-commit lines)
  (let ([commit (car lines)]
        [header (map parse-header
                     (take-while (^x (not (string=? x "")))
                                 lines))]
        [diff (filter (cut string-prefix? ":" <>) lines)])
    (list commit header (filter-map parse-diff diff))))

(define (%git-log-raw)
  (let ([raw-commits
         (group-lines (cut string-prefix? "commit " <>)
                      (process-output->string-list
                       (list "git" "-C" (*git-dir*) "log"
                             "--pretty=raw" "--raw" "--no-abbrev")))])
    (map parse-raw-commit raw-commits)))

(define (git-log-raw)
  (git-dir-cache-through! *git-log-raw-cache* %git-log-raw))

(define (post-commits id)
  (filter (lambda (commit)
            (find (lambda (diff)
                    (string=? (car diff) id))
                  (third commit)))
          (git-log-raw)))

(define (commit->date commit)
  (let ([time (third (cdr (assoc 'author (second commit))))])
    (let ([time (seconds->time (first time))]
          [tz-offset (second time)])
      (time-utc->date time tz-offset))))

(define (new-post-path)
  (let ([paths (process-output->string-list
                (list "git" "-C" (*git-dir*)
                      "diff-files"
                      "--name-only"
                      "--diff-filter=A"
                      "--ita-invisible-in-index"))])
    (cond
     [(length=? paths 0) #f]
     [(length=? paths 1) (car paths)]
     [else
      (raise (make-error #"Could not handle more than one new post: ~|paths|"))])))

; `date` is used for generating date tag because it's supposed to be
; human readable and should be localized (abusing date's locale
; support). ID could be generatedly internally.
(define (add-new-post)
  (let ([path (process-output->string
               '(date +%Y/%Y-%m-%dT%H_%M_%S.txt))]
        [date (process-output->string
               '(date))])
    (make-directory* (sys-dirname
                      (build-path (*git-dir*) path)))
    (with-output-to-file
        (build-path (*git-dir*) path)
      (lambda ()
        (print "author " (sys-uid->user-name (sys-getuid)))
        (print "date " date)))
    (do-process (list "git" "-C" (*git-dir*) "add" "-N" path))))

(define (commit-new-post)
  (define (ellipsis string max-length)
    (let ([len (string-length string)])
      (if (and (*sidebar-max-length*)
               (> len max-length))
          (string-append (substring string 0 max-length) "…")
          string)))

  (let* ([path (new-post-path)]
         ; validate valid id from path, just in case
         [id (post-path->id path)]
         [post (call-with-input-file
                   (build-path (*git-dir*) path)
                 (lambda (port)
                   (read-post id path port)))])
    (do-process (list "git" "-C" (*git-dir*)
                      "commit" "--edit"
                      "-m" (or (post-title post)
                               (ellipsis (post-body post) 70))
                      path))))

(define (delete-new-post)
  (let ([path (new-post-path)])
    ; this is done so that we can "undo" the operation because the
    ; content is hashed and kept in git object database.
    (do-process (list "git" "-C" (*git-dir*) "add" path))
    (do-process (list "git" "-C" (*git-dir*)
                      "rm" "--force"
                      path))))

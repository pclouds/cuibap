;; -*- indent-tabs-mode: nil -*-
(define-module cuibap.cli
  (use cuibap)
  (use cuibap.params)
  (use cuibap.storage)
  (use file.util)
  (use gauche.parseopt)
  (use gauche.process)
  (use srfi-1)
  (use srfi-13)
  (use srfi-39)
  (use srfi-69)
  (export command))
(select-module cuibap.cli)

(define *commands* (make-hash-table))

(define-syntax define-cmd
  (syntax-rules ()
    [(_ (name . args) help body ...)
     (hash-table-set! *commands* (symbol->string (quote name))
                      (cons help
                            (lambda args body ...)))]

    [(_ (name args ...) help body ...)
     (hash-table-set! *commands* (symbol->string (quote name))
                      (cons help
                            (lambda (args ...) body ...)))]))

(define (name->proc name)
  (cdr (hash-table-get *commands* name '("" . #f))))

(define (expand-cmdname name)
  (define (match-one re)
    (let ([matches (filter re (command-list))])
      (cond
       [(length=? matches 1) (begin
                               (print "Expanding " name " to " (car matches))
                               (first matches))]
       [(length>? matches 1) (begin
                               (print "Ambiguous commands " matches)
                               #f)]
       [else #f])))

  (or (match-one (string->regexp
                  (string-append "^" name ".*")))
      (and (string-contains name "-")
           (match-one (string->regexp
                       (string-append "^"
                                      (regexp-replace #/-/ name ".*-")
                                      ".*"))))
      name))

(define (get-help-text name)
  (let ([val (hash-table-get *commands* name #f)])
    (if val (car val) "No help available")))

(define (command name . opts)
  (let ([proc (or (name->proc name)
                  (name->proc (expand-cmdname name)))])
    (if proc
        (apply proc opts)
        (print #"Command ~name is not found."))))

(define (command-list)
  (hash-table-keys *commands*))

(define-cmd (help)
  "Print help usage"
  (let ([cmds (command-list)])
    (print "Supported commands:\n")
    (for-each (lambda (name)
                (format #t " * ~20a ~a\n"
                        name
                        (get-help-text name)))
              (sort cmds))))

(define-cmd (update-all)
  "Regenerate all HTML pages"
  (queue-all-pages)
  (gen-pages :verbose #f))

(define-cmd (update . args)
  "Regenerate HTML pages for modified posts"
  (queue-git-updates :head (if (length>? args 0)
                               (car args)
                               "HEAD"))
  (gen-pages))

(define-cmd (update-main . args)
  "Regenerate the home HTML page"
  (let-args args
      ([max-entries  "max-entries=i" (*max-entries*)])
    (parameterize ([*max-entries* max-entries])
      (queue-main-page)
      (gen-pages))))

(define-cmd (update-tags)
  "Regenerate HTML tag pages"
  (queue-tag-pages)
  (gen-pages))

(define-cmd (new-post)
  "Add (and edit) a new post"
  (if (new-post-path)
      (raise (make-error "A new post is already being prepared")))
  (if (add-new-post)
      (command "edit-post")))

(define-cmd (edit-post)
  "Edit the new post"
  (let ([path (new-post-path)])
    (unless path
      (raise (make-error "No new post is being prepared")))
    (do-process (list (or (sys-getenv "EDITOR") "vi")
                      (build-path (*git-dir*) path)))
    (command "update-main")))

(define-cmd (commit-post)
  "Commit the new post"
  (let ([path (new-post-path)])
    (unless path
      (raise (make-error "No new post is being prepared")))
    (if (commit-new-post)
        (begin
          (queue-git-updates :head "HEAD^")
          (gen-pages)))))

(define-cmd (cancel-post)
  "Cancel and delete the new post"
  (unless (new-post-path)
      (raise (make-error "No new post is being prepared")))
  (delete-new-post))

(define-cmd (update-template)
  "Update static files in output directory"
  (copy-template (*output-dir*)))

(define-cmd (publish)
  "Commit all changes in the output directory"
  (let ([summary (process-output->string
                  (list "git" "-C" (*git-dir*) "show"
                        "-s" "--date=short"
                        "--pretty=format:%h (%s - %ad)"))])
    (and (do-process (list "git" "-C" (*output-dir*) "add" "-A"))
         (do-process (list "git" "-C" (*output-dir*) "commit" "-m" summary)))))

(define (execute dir args)
  (do-process (if (pair? args)
                  args
                  (begin
                    (print #"Entering '~dir'...")
                    (or (sys-getenv "SHELL") "/bin/sh")))
              :directory dir))

(define-cmd (src . args)
  "Execute a command (or a new shell) in the source directory"
  (execute (*git-dir*) args))

(define-cmd (dst . args)
  "Execute a command (or a new shell) in the output directory"
  (execute (*output-dir*) args))

(define-cmd (both-src-dst . args)
  "Execute a command in both source and output directories"
  (if (pair? args)
      (begin (execute (*git-dir*) args)
             (execute (*output-dir*) args))
      (raise (make-error "A command is required"))))

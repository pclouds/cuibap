;; -*- indent-tabs-mode: nil -*-
(define-module cuibap.params
  (use srfi-39)
  (export *blog-description*
          *blog-title*
          *contacts*
          *content-type*
          *css-url*
          *extra-html-head*
          *extra-html-head-proc*
          *favicon-url*
          *generator*
          *git-dir*
          *max-entries*
          *output-dir*
          *print-css-url*
          *sidebar-max-length*
          *url-prefix*))
(select-module cuibap.params)

(define *blog-description* (make-parameter "Phầm mềm Blog cây nhà lá vườn"))
(define *blog-title* (make-parameter "Blog của Cùi Bắp"))
(define *contacts* (make-parameter ""))
(define *content-type* (make-parameter "application/xhtml+xml; charset=utf-8"))
(define *css-url* (make-parameter "styles/nb_default.css"))
(define *extra-html-head* (make-parameter '()))
(define *extra-html-head-proc* (make-parameter (lambda () '())))
(define *favicon-url* (make-parameter "images/favicon.ico"))
(define *generator* (make-parameter "Cùi Bắp 0.2"))
(define *git-dir* (make-parameter "data"))
(define *max-entries* (make-parameter 10))
(define *output-dir* (make-parameter "build"))
(define *print-css-url* (make-parameter "styles/print.css"))
;; Max length of text (usually titles) on the side bar, the rest will
;; be substituted by an ellipsis. Set to #f to always show full text.
(define *sidebar-max-length* (make-parameter 30))

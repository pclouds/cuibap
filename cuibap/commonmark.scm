;; -*- indent-tabs-mode: nil -*-
(define-module cuibap.commonmark
  (use cuibap.storage)
  (use cuibap.url)
  (use gauche.process)
  (use gauche.threads)
  (use rfc.base64)
  (use srfi-1)
  (use srfi-13)
  (use srfi-6)
  (use sxml.serializer)
  (use sxml.ssax)
  (use sxml.sxpath)
  (use sxml.tools)
  (use sxml.tree-trans)
  (export markdown->html markdown->sxml fixup-html-blobs))
(select-module cuibap.commonmark)

(define (drop-attributes content)
  (filter (lambda (x)
	    (not (and (pair? x)
		      (eq? (car x) '@))))
	  content))

(define (tighten-item li-node)
  (cons (car li-node)
	(apply append (map (lambda (node)
			     (if (and (pair? node)
				      (eq? (car node) 'p))
				 (cdr node)
				 (list node)))
			   (cdr li-node)))))

(define (rip-text node)
  (apply string-append
	 (sxml:clean-feed
	  (pre-post-order node
			  `((@ . ,(lambda node '()))
			    (*text* . ,(lambda (tag . content) content))
			    (*default* . ,(lambda (tag . content) content)))))))
(define (rewrite-uri uri)
  (define (strip-prefix str prefix)
    (let ([pref-len (string-length prefix)]
          [str-len (string-length str)])
      (if (and (>= str-len pref-len)
               (string=? (substring str 0 pref-len) prefix))
          (substring str pref-len str-len)
          #f)))
  (cond
   ;; FIXME tex://
   [(post-id? uri) (url:post uri)]
   [(strip-prefix uri "file:") => (cut url:file <>)]
   [(strip-prefix uri "lastfm:")
    => (cut string-append "https://www.last.fm/" <>)]
   [else uri]))

(define (sxml->html tree)
  (pre-post-order
   tree
   `((md:code_block . ,(lambda node
			 (let* ([content (sxml:content node)]
				[info (sxml:attr node 'info)])
			   (if info
			       `(pre (code (@ (class
					       ,(string-append "language-"
							       (regexp-replace #/ .*/ info ""))))
					   ,@content))
			       `(pre (code ,@content))))))
     (md:text . ,(lambda node (apply string-append (sxml:content node))))
     (md:code . ,(lambda node
		   (let ([content (sxml:content node)])
		     `(code ,@content))))
     (*text* . ,(lambda (tag content) content))
     (md:html_block . ,(lambda node
			 `(pre (@ (html "base64")) ,@(map base64-encode-string (sxml:content node)))))
     ;; sxml serializer does not understand html inline elements yet
     ;; and will not treat it differently than block elements, so
     ;; stick to <pre> to simplify post processing.
     (md:html_inline . ,(lambda node
			  `(pre (@ (html "base64")) ,@(map base64-encode-string (sxml:content node)))))
     (md:link . ,(lambda node
		   (let ([dest (sxml:attr node 'destination)]
			 [title (or (sxml:attr node 'title) "")]
			 [content (drop-attributes (cdr node))])
		     `(a (@ (href ,(rewrite-uri dest))
			    ,(if (string-null? title) '() (list 'title title)))
			 ,@content))))
     (md:image . ,(lambda node
		    (let ([dest (sxml:attr node 'destination)]
			  [title (sxml:attr node 'title)]
			  [content (drop-attributes (cdr node))])
		      `(img (@ (src ,(rewrite-uri dest))
			       (alt ,(rip-text node))
			       ,(if (and (string? title)
					 (not (string-null? title)))
				    `(title ,title)))))))
     (md:paragraph . ,(lambda (tag . content)
			`(p ,@content)))
     (md:block_quote . ,(lambda (tag . content)
			  `(blockquote ,@content)))
     (md:list . ,(lambda node
		   (let ([type (sxml:attr node 'type)]
			 [tight (string=? (sxml:attr node 'tight) "true")]
			 [start (sxml:attr node 'start)]
			 [content (drop-attributes (cdr node))])
		     (if (string=? type "bullet")
			 `(ul ,@(if tight (map tighten-item content) content))
			 `(ol (@ ,(if (and (string? start)
					   (not (string=? start "1")))
				      `(start ,start) '()))
			      ,@(if tight (map tighten-item content) content))))))
     (md:item . ,(lambda (tag . content)
		   `(li ,@content)))
     (md:heading . ,(lambda node
		      `(,(string->symbol (string-append "h" (sxml:attr node 'level)))
			,@(drop-attributes (cdr node)))))
     (md:thematic_break . ,(lambda (tag)
			     '(hr)))
     (md:linebreak . ,(lambda (tag)
			'(br)))
     (md:softbreak . ,(lambda (tag)
			"\n"))
     (md:emph . ,(lambda (tag . content)
		   `(em ,@content)))
     (md:strong . ,(lambda (tag . content)
		     `(strong ,@content)))
     (md:document . ,(lambda (tag . content)
		       `(body ,@content)))
     (*default* . ,(lambda args args)))))

(define (string->sxml xml)
  (call-with-input-string xml
    (lambda (port)
      (ssax:xml->sxml port '((md . "http://commonmark.org/xml/1.0"))))))

(define (fixup-html-blobs html)
  (regexp-replace-all #/<pre html="base64">([^<]*)<\/pre>/
		      html
		      (lambda (re)
			(base64-decode-string (rxmatch-substring re 1)))))

(define (consumer-thunk proc)
  (lambda ()
    (let ([stdout (process-output proc)]
          [out (open-output-string)])
      (copy-port stdout out)
      (close-port stdout)
      (get-output-string out))))

(define (filter-process cmd input)
  (let ([proc (run-process cmd :redirects '((< 0 stdin)
                                            (> 1 stdout)))])
    (let ([consumer (thread-start! (make-thread (consumer-thunk proc)))]
          [stdin (process-input proc)])
      (copy-port (open-input-string input) stdin)
      (close-port stdin)
      (process-wait proc)
      (thread-join! consumer))))

(define (markdown->sxml text)
  (sxml:clean
   (sxml->html
    (string->sxml
     (filter-process '(cmark -t xml) text)))))

(define (markdown->html text)
  (fixup-html-blobs
   (srl:sxml->html
    ((sxpath "/body/*") (markdown->sxml text)))))

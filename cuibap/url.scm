;; -*- indent-tabs-mode: nil -*-
(define-module cuibap.url
  (use cuibap.params)
  (use cuibap.storage)
  (use gauche.unicode)
  (use srfi-28)
  (use srfi-39)
  (use text.tr)
  (export *url-prefix*

          url:rel

          url:archives
          url:file
          url:home
          url:month
          url:post
          url:tag
          url:year))
(select-module cuibap.url)

(define *url-prefix* (make-parameter ""))

(define (url:rel absolute-url)
  (string-append (*url-prefix*) absolute-url))

(define (url:archives)
  (url:rel "archives/index.html"))

(define (url:file path)
  (url:rel #"files/~path"))

(define (url:home)
  (url:rel "index.html"))

(define (url:month month year)
  (url:rel (format #f "archives/~4,'0d/~2,'0d/index.html" year month)))

(define (url:post id)
  (let ([month (post-id->month id)]
        [year (post-id->year id)])
    (format #f "~a#~a" (url:month month year) id)))

(define (ascii-ize name)
  (cond
   [(string=? name "日本語") "nihongo"]
   [else
    (string-tr (string-downcase name)
               " đáàảãạâấầẩẫậăắằẳẵặéèẻẽẹêếềểễệíìỉĩịóòỏõọôốồổỗộơớờởỡợúùủũụưứừửữựýỳỷỹỵ"
               "-daaaaaaaaaaaaaaaaaeeeeeeeeeeeiiiiiooooooooooooooooouuuuuuuuuuuyyyyy")]))

(define (url:tag tag)
  (url:rel (format #f "archives/tags/~a.html" (ascii-ize tag))))

(define (url:year year)
  (url:rel (format #f "archives/~4,'0d/index.html" year)))

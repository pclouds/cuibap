;; -*- indent-tabs-mode: nil -*-
(define-module cuibap.nanoblogger
  (use srfi-1)
  (use srfi-9)
  (use srfi-13)
  (use srfi-26)
  (export read-post
          <post>
          post?
          post-author
          post-body
          post-date
          post-html-body
          post-html-body-set!
          post-id
          post-path
          post-tags
          post-title))
(select-module cuibap.nanoblogger)

(define-constant SEPARATOR-LINE "-----")

(define-record-type <post> make-post post?
  (id post-id)
  (path post-path)
  (author post-author)
  (title post-title)
  (tags post-tags)
  (date post-date)
  (body post-body)
  (html-body post-html-body post-html-body-set!))

(define (parse-header-value name value)
  (case name
    [(TAGS) (filter (lambda (x)
                      (not (string=? x "")))
                    (map string-trim-both (string-split value ",")))]
    [else value]))

(define (header->acons line)
  (let ([match (rxmatch #/^([^:]+): *(.*)$/ line)])
    (let ([name (string->symbol (match 1))]
          [value (match 2)])
      (cons name (parse-header-value name value)))))

(define (parse-body body)
  (receive (body trailer)
      (break (cut string=? <> SEPARATOR-LINE)
             ;; ignore the leading ----- and "BODY:" lines
             (cddr body))
    (string-join body "\n")))

(define (%nanopost->alist strings)
  (receive (header body)
      (break (cut string=? <> SEPARATOR-LINE)
             strings)
    (let ([body (parse-body body)]
          [header-tags (map header->acons header)])
      (alist-cons 'BODY body header-tags))))

(define (%gitpost->alist strings)
  (define (parse-header tags)
    (map (lambda (x)
           (let* ([l (string-split x " " 1)]
                  [name (string->symbol (string-upcase (car l)))]
                  [value (cadr l)])
             (cons name (parse-header-value name value))))
         tags))

  (define (parse-title body alist)
    (values (cddr body)
            (cons (cons 'TITLE (car body)) alist)))

  (define (parse-body body alist)
    (cons (cons 'BODY (string-join body "\n")) alist))

  (receive (header body)
      (break (cut string=? <> "") strings)
    (if (pair? (cddr body))
        (call-with-values
            (^[] (parse-title (cdr body)
                              (parse-header header)))
          parse-body)
        `((TITLE . #f)
          (BODY . ,(cadr body))
          ,@(parse-header header)))))

(define (nanopost->alist port)
  (let ([strings (port->string-list port)])
    (if (and (pair? strings) (#/^author / (car strings)))
        (%gitpost->alist strings)
        (%nanopost->alist strings))))

(define (read-post id path port)
  (let ([alist (nanopost->alist port)])
    (make-post id
               path
               (cdr (assoc 'AUTHOR alist))
               (cdr (assoc 'TITLE alist))
               (if-let1 pair (assoc 'TAGS alist)
                        (cdr pair)
                        '())
               (cdr (assoc 'DATE alist))
               (cdr (assoc 'BODY alist))
               #f)))

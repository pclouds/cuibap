(define-module cuibap.tex
  (use file.util)
  (use gauche.process)
  (use rfc.base64)
  (export tex->uri))
(select-module cuibap.tex)

(define (with-temp-dir template proc)
  (let ([tmpdir (sys-mkdtemp template)])
    (let ([result (proc tmpdir)])
      (remove-directory* tmpdir)
      result)))

(define (tex->uri tex)
  (with-temp-dir "cuibap"
    (lambda (tmpdir)
      (with-output-to-file (build-path tmpdir "input.tex")
	(lambda ()
	  (print "\\documentclass[12pt]{standalone}"
		 "\\begin{document}"
		 tex
		 "\\end{document}")))
      (do-process! `(pdflatex -interaction=nonstopmode "input.tex")
		   :directory tmpdir)
      (do-process! `(convert -density 100 "input.pdf" "output.png")
		   :directory tmpdir)
      (with-input-from-file (build-path tmpdir "output.png")
	(lambda ()
	  (string-append "data:image/png;base64,"
			 (with-output-to-string
			   (lambda ()
			     (base64-encode :line-width #f)))))))))

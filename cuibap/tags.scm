;; -*- indent-tabs-mode: nil -*-
(define-module cuibap.tags
  (use cuibap.nanoblogger)
  (use gauche.collection)
  (use srfi-1)
  (export tag-list
          collect-tags!))
(select-module cuibap.tags)

(define *tag-alist* '())

(define (delete-post tag-alist id)
  (filter (lambda (pair)
            (not (eq? (cdr pair) id)))
          tag-alist))

(define (add-post alist post)
  (let* ([id (post-id post)])
    (append (map (cut cons <> id)
                 (post-tags post))
            alist)))

(define (replace-post alist post)
  (add-post (delete-post alist
                         (post-id post))
            post))

(define (%get-tags alist)
  (map (lambda (list)
         (cons (car (first list))
               (delete-duplicates (map cdr list))))
       (group-collection alist
                         :key car
                         :test string-ci=?)))

(define (tag-list)
  (%get-tags *tag-alist*))

(define (collect-tags! post)
  (set! *tag-alist* (replace-post *tag-alist* post)))

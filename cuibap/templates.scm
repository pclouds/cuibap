;; -*- indent-tabs-mode: nil -*-
(define-module cuibap.templates
  (use cuibap.calendar)
  (use cuibap.commonmark)
  (use cuibap.nanoblogger)
  (use cuibap.params)
  (use cuibap.storage)
  (use cuibap.url)
  (use gauche.process)
  (use srfi-1)
  (use srfi-13)
  (use srfi-19)
  (use srfi-28)
  (use srfi-39)
  (use srfi-42)
  (use srfi-69)
  (use sxml.serializer)
  (export sxml->html
          sxml:archive-page
          sxml:main-page
          sxml:month-page
          sxml:tag-page
          sxml:year-page))
(select-module cuibap.templates)

(define (sxml:side-box name children)
  `((div (@ (class "sidetitle")) ,name)
    (div (@ (class "side")) ,@children)))

(define *blobs* (make-parameter #f))
(define *blob-counter* (make-parameter 0))

(define (sxml:blob content)
  (let ([id #"blob-~(*blob-counter*)"])
    (set! (*blob-counter*) (+ (*blob-counter*) 1))
    (hash-table-set! (*blobs*) id content)
    `(& ,id)))

(define (substitute-blobs content)
  (regexp-replace-all (string->regexp "&(blob-[0-9]+);")
                      content
                      (lambda (match)
                        (hash-table-get (*blobs*) (match 1)))))

(define (sxml->html thunk)
  (parameterize ([*blobs* (make-hash-table)]
                 [*blob-counter* 0])
    (let ([content (srl:sxml->html (thunk))])
      (if (> (*blob-counter*) 0)
          (substitute-blobs content)
          content))))

(define (sidebar-text string)
  (let ([len (string-length string)])
    (if (and (*sidebar-max-length*)
             (> len (*sidebar-max-length*)))
        (string-append (substring string 0 (*sidebar-max-length*)) "…")
        string)))

(define (sxml:layout . body)
  `(html
    (head
     (meta (@ (http-equiv "Content-Type") (content ,(*content-type*))))
     (meta (@ (name "generator") (content ,(*generator*))))
     (meta (@ (name "robots") (content "all")))
     (meta (@ (name "revisit-after") (content "31 days")))
     (meta (@ (name "viewport") (content "width=device-width, initial-scale=1.0")))
     (title ,(*blog-title*))
     (link (@ (rel "stylesheet")
              (href ,(url:rel (*css-url*)))
              (type "text/css")
              (media "all")))
     (link (@ (rel "stylesheet")
              (href ,(url:rel (*print-css-url*)))
              (type "text/css")
              (media "print")))
     (link (@ (rel "shortcut icon")
              (href ,(url:rel (*favicon-url*)))))
     (script (@ (src "https://code.jquery.com/jquery-3.3.1.min.js")
                (integrity "sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=")
                (crossorigin "anonymous")))
     (script (@ (src "https://code.jquery.com/ui/1.12.1/jquery-ui.min.js")
                (integrity "sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU=")
                (crossorigin "anonymous")))
     ,@(*extra-html-head*)
     ,@((*extra-html-head-proc*)))
    (body
     (div (@ (id "container") (class "clearfix"))
          (div (@ (style "float: right"))
               (img (@ (id "avatar")
                       (src ,(url:rel "images/avatar.png"))
                       (onclick "$('#links').toggle('slide', {direction: 'right'})"))))
          (div (@ (id "header") (class "clearfix"))
               (h1
                (a (@ (href ,(url:home))
                      (accesskey "1"))
                   ,(*blog-title*)))
               (span (@ (class "description"))
                     ,(*blog-description*)))
          ,@body)
     (div (@ (id "footer") (style "clear: both"))
          (div (@ (class "metainfo"))
               ,#"Được bảo kê bởi ~(*generator*)")
          (div (@ (class "menu"))
               (a (@ (href "#top")) "Lên đầu")
               (br))))))

(define (sxml:tag-links tags)
  (sxml:side-box "Phân loại"
                 (append-map (lambda (pair)
                               `((a (@ (href ,(url:tag (car pair)))) ,(car pair))
                                 " (" ,(length (cdr pair)) ") "))
                             (sort tags
                                   string-ci-comparator
                                   car))))

(define (sxml:archive-links year-counts-alist)
  (sxml:side-box "Kho"
                 `((a (@ (href ,(url:archives))) "Danh mục kho")
                   (br)
                   ,@(append-map (lambda (x)
                                   (let ([year (car x)]
                                         [count (cdr x)])
                                     `((a (@ (href ,(url:year year))) ,year)
                                       ,#" (~|count|)"
                                       (br))))
                                 year-counts-alist))))

(define (sxml:contacts contacts)
  (sxml:side-box "Liên hệ" contacts))

(define (sxml:blog-status :key num-entries last-update)
  (sxml:side-box "Tình trạng"
                 `(,#"Tổng số bài: ~|num-entries|"
                   (br)
                   ,#"Lần cập nhật cuối: ~|last-update|")))

(define (sxml:recent-entries entries)
  (sxml:entries "Bài mới" entries))

(define (sxml:archive-entries entries)
  (sxml:entries "Bài viết" entries))

(define (sxml:entries title entries)
  (sxml:side-box title
                 (append-map (lambda (post)
                               `((a (@ (href ,#"#~(post-id post)"))
                                    ,(sidebar-text (post-title post)))
                                 (br)))
                             (filter post-title entries))))

(define (sxml:syndicate)
  `((div (@ (class "syndicate"))
         "Theo dõi "
         (a (@ (href ,(url:rel "rss.xml"))) "RSS")
         " "
         (a (@ (href ,(url:rel "atom.xml"))) "Atom"))))

(define (sxml:sidebar . boxes)
  `(div (@ (id "links"))
        ,@(apply append boxes)))

(define (sxml:home-page entries)
  `(div (@ (id "content"))
        (div (@ (id "wrap"))
             (a (@ (id "top")))
             (div (@ (class "blog"))
                  ,@entries))))

(define (sxml:month-archive blog-entries
                            :key month year
                            prev-month next-month)
  (define (pair->link x)
    `(a (@ (href ,(cdr x))) ,(car x)))

  (define (year-line)
    (intersperse " > "
                 (map pair->link
                      `(("Trang chính" . ,(url:home))
                        ("Kho" . ,(url:archives))
                        (,year . ,(url:year year))))))

  (define (month-line)
    (intersperse " "
                 (cond-list
                  [prev-month `(a (@ (href ,(url:month prev-month year)))
                                  ,prev-month)]
                  [#t month]
                  [next-month `(a (@ (href ,(url:month next-month year)))
                                  ,next-month)])))

  `(div (@ (id "content"))
        (div (@ (id "wrap"))
             (a (@ (id "top")))
             (h2 ,#"Kho tháng ~|month|/~|year|")
             (div (@ (id "menu"))
                  ,@(year-line) " > "
                  (br)
                  ,@(month-line))
             (div (@ (class "archives"))
                  ,@blog-entries))))

(define (sxml:year-archive year months posts
                           :key
                           (prev-year #f)
                           (next-year #f))
  (define (month->link month)
    (let ([post-count (length (posts-by-month month year))])
      `((a (@ (href ,(url:month month year)))
           ,#"Tháng ~month")
        ,#" (~|post-count|)"
        (br))))

  (define (post->link post)
    (let* ([id (post-id post)]
           [month (post-id->month id)]
           [year (post-id->year id)])
      `((a (@ (href ,(url:month month year)))
           ,#"~month / ~year")
        " - "
        (a (@ (href ,(url:post id)))
           ,(post-title post))
        ,@(cond-list
           [(length>? (post-tags post) 0)
            @ (cons " - "
                    (intersperse ", "
                                 (map (lambda (tag)
                                        `(a (@ (href ,(url:tag tag))) ,tag))
                                      (post-tags post))))])
        (br))))

  `(div (@ (id "altcontent"))
        (div (@ (id "wrap"))
             (a (@ (id "top")))
             (h2 "Kho")
             (div (@ (id "menu"))
                  (a (@ (href ,(url:home))) "Trang chính")
                  " > "
                  (a (@ (href ,(url:archives))) "Kho")
                  " > "
                  (br)
                  ,@(cond-list
                     [prev-year `(a (@ (href ,(url:year prev-year))) ,prev-year)]
                     [#t year]
                     [next-year `(a (@ (href ,(url:year next-year))) ,next-year)]))
             (div (@ (class "archives"))
                  (a (@ (id "date")))
                  (strong "Xem theo ngày")
                  (div ,@(append-map month->link months))
                  (br)
                  (a (@ (id "date")))
                  (strong "Xem theo bài")
                  (div ,@(append-map post->link (filter post-title posts)))))))

(define (sxml:archive year-counts)
  (define (year->link year-count)
    (let ([year (car year-count)]
          [count (cdr year-count)])
      `((a (@ (href ,(url:year year)))
           ,#"Năm ~year")
        ,#" (~|count|)"
        (br))))

  `(div (@ (id "altcontent"))
        (div (@ (id "wrap"))
             (a (@ (id "top")))
             (h2 "Kho")
             (div (@ (id "menu"))
                  (a (@ (href ,(url:home))) "Trang chính")
                  " > "
                  (br)
                  (a (@ (href ,(url:archives))) "Kho")
                  " > ")
             (div (@ (class "archives"))
                  (a (@ (id "date")))
                  (strong "Xem theo năm")
                  (div ,@(append-map year->link year-counts))))))

(define (sxml:tag-archive tag-name posts)
  (define (post->link post)
    (let* ([id (post-id post)]
           [month (post-id->month id)]
           [year (post-id->year id)])
      `((a (@ (href ,(url:month month year))) ,#"~month / ~year")
        " - "
        (a (@ (href ,(url:post id))) ,(post-title post))
        (br))))

  `(div (@ (id "altcontent"))
        (div (@ (id "wrap"))
             (a (@ (id "top")))
             (h2 "Kho")
             (div (@ (id "menu"))
                  (a (@ (href ,(url:home))) "Trang chính")
                  " > "
                  (a (@ (href ,(url:archives))) "Kho")
                  " > "
                  (br)
                  ,tag-name)
             (div (@ (class "archives"))
                  (a (@ (id "date")))
                  (strong "Xem theo bài")
                  (div ,@(append-map post->link posts))))))

(define (sxml:entry post :key (updates #f))
  (define (show-updates)
    (let* ([commits (post-commits (post-id post))])
      (if (and updates (> (length commits) 1))
          `((br)
            (i
             ,(format #f "Cập nhật ~a lần. " (- (length commits) 1))
             "Lần cuối: "
             ,(date->string (commit->date (car commits)) "~c")))
          '())))

  (define (tags)
    (let ([tags (post-tags post)])
      (if (length>? tags 0)
          (append '(" | ")
                  (intersperse ", "
                               (map (lambda (tag)
                                      `(a (@ (href ,(url:tag tag))) ,tag))
                                    tags)))
          '())))

  `(div (@ (class "item"))
        (a (@ (id ,(post-id post))))
        (h2 (@ (class "date"))
            ,(post-date post))
        (div (@ (class "blogbody"))
             ,@(if (post-title post)
                   `((h3 (@ (class "title"))
                         ,(post-title post)))
                   '())
             (div (@ (class "item-description"))
                  ,(sxml:blob (post-html-body post))
                  ,@(show-updates)))
        (div (@ (class "posted"))
             (br)
             "Tác giả: "
             (span (@ (class "item-creator"))
                   ,(post-author post))
             " | "
             (a (@ (class "link")
                   (href ,(url:post (post-id post))))
                "Liên kết tĩnh")
             ,@(tags))))

; Note, vi_VN locale prints year and month in two separate lines while
; C locale does it in one. This code goes for vi_VN, naturally.
(define (sxml:calendar ids :key (month #f) (year #f))
  (define (linkify-day day)
    (let ([a (assoc day ids)])
      (if a
          `(a (@ (href ,#"#~(cdr a)")) ,day)
          day)))

  (let* ([now (current-date)]
         [month (or month (date-month now))]
         [year (or year (date-year now))]
         [weeks (calendar (make-date 0 0 0 0 0
                                     month year
                                     (date-zone-offset now)))])
    `(div (@ (class "calendar"))
          (table (@ (border 0)
                    (cellspacing 4)
                    (cellpadding 0)
                    (summary "Calendar with links to days with entries"))
                 (caption (@ (class "calendarhead"))
                          ,#"~(month-name month) ~|year|")
                 (tr
                  ,@(map (lambda (name)
                           `(th (@ (style "text-align: center"))
                                (span (@ (class "calendarday"))
                                      ,name)))
                         (map weekday-name
                              (list-ec (: i 1 8) (modulo i 7)))))
                 ,@(map (lambda (week)
                          `(tr
                            ,@(map (lambda (day)
                                     (if day
                                         `(td (@ (style "text-align: center"))
                                              (span (@ (class "calendar"))
                                                    ,(linkify-day day)))
                                         '(td)))
                                   week)))
                        weeks)))))

(define (sxml:main-page posts tags year-counts contacts total-num-entries)
  (sxml:layout
   (sxml:sidebar
    (sxml:recent-entries (reverse posts))
    (sxml:tag-links tags)
    (sxml:archive-links year-counts)
    (sxml:syndicate)
    (sxml:contacts contacts)
    (sxml:blog-status :num-entries total-num-entries
                      :last-update (date->string (current-date) "~1")))
   (sxml:home-page (map sxml:entry (reverse posts)))))

(define (sxml:month-page month year posts id-list)
  (sxml:layout
   (sxml:sidebar (list (sxml:calendar id-list
                                      :month month
                                      :year year))
                 (sxml:archive-entries (reverse posts)))
   (sxml:month-archive (map (cut sxml:entry <> :updates #t) (reverse posts))
                       :month month
                       :year year
                       :prev-month (get-prev-month month year)
                       :next-month (get-next-month month year))))

(define (sxml:year-page year months posts)
  (sxml:layout
   (sxml:year-archive year months posts)))

(define (sxml:archive-page year-counts)
  (sxml:layout
   (sxml:archive year-counts)))

(define (sxml:tag-page tag-name posts)
  (sxml:layout
   (sxml:tag-archive tag-name posts)))

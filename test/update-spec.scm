;;; the equivalent of spec_tests.py in cmark. Read spec.txt and
;;; extract all test cases to a list
(use cuibap.commonmark)
(use gauche.process)
(use gauche.threads)
(use srfi-1)
(use sxml.serializer)
(use sxml.tools)
(use text.tr)

(define quote-line (make-string 32 #\`))

(define (skip-until match)
  (let loop ([line (read-line)])
    (if (or (eof-object? line) (string=? line match))
	line
	(loop (read-line)))))

(define (collect-until match)
  (let loop ([line (read-line)]
	     [result '()])
    (if (or (eof-object? line) (string=? line match))
	(if (null? result)
	    result
	    (string-join (reverse result) "\n"))
	(loop (read-line) (cons line result)))))

(define (decode-tab text)
  (string-tr text "→" "\t"))

(define (encode-tab text)
  (string-tr text "\t" "→"))

(define (cmark->html xml)
  (fixup-html-blobs
   (srl:sxml->html
    (sxml:clean
     (markdown->sxml xml)))))

(define (collect-test-blocks)
  (let ([start-line (string-append quote-line " example")]
	[end-line quote-line])
    (let loop ([result '()])
      (if (eof-object? (skip-until start-line))
	  (reverse result)
	  (loop (cons (decode-tab (collect-until end-line)) result))))))

(define (split-test-block block)
  (with-input-from-string block
    (lambda ()
      (let* ([first (collect-until ".")]
	     [second (port->string (current-input-port))])
	(list first second)))))

(define (create-spec-scm input output)
  (let ([test-blocks (with-input-from-file input collect-test-blocks)])
    (with-output-to-file output
      (lambda ()
	(pprint (map split-test-block test-blocks))))))

(define (do-create-new-spec)
  (let ([start-line (string-append quote-line " example")]
	[end-line quote-line])
    (let loop ([plain-text (collect-until start-line)])
      (if (not (null? plain-text))
	  (begin
	    (print plain-text)
	    (let ([test-data (collect-until end-line)])
	      (if (not (null? test-data))
		  (begin
		    (print start-line)
		    (let ([data (split-test-block test-data)])
		      (print (first data))
		      (print ".")
		      (print (encode-tab (cmark->html (decode-tab (first data))))))
		    (print end-line)
		    (loop (collect-until start-line))))))))))

(define (create-new-spec input output)
  (with-output-to-file output
    (lambda ()
      (with-input-from-file input
	do-create-new-spec))))

(create-new-spec "spec.txt" "new-spec.txt")
(sys-rename "new-spec.txt" "spec.txt")

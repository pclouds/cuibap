;; -*- indent-tabs-mode: nil -*-
(define-module cuibap
  (use cuibap.commonmark)
  (use cuibap.nanoblogger)
  (use cuibap.params)
  (use cuibap.storage)
  (use cuibap.tags)
  (use cuibap.templates)
  (use cuibap.url)
  (use file.util)
  (use gauche.process)
  (use srfi-1)
  (use srfi-19)
  (use srfi-39)
  (use srfi-69)
  (use text.progress)
  (use util.match)
  (export archive-page
          copy-template
          gen-pages
          load-config
          load-post
          main-page
          month-page
          queue-all-pages
          queue-git-updates
          queue-main-page
          queue-tag-pages
          year-page))
(select-module cuibap)

(define *queued-pages* '())

(define *cached-posts* (make-hash-table))

(define (%load-post id)
  (call-with-input-post
      id
    (lambda (port)
      (read-post id (post-id->path id) port))))

(define (load-post id)
  (let ([value (hash-table-get *cached-posts* id #f)])
    (unless value
        (set! value (%load-post id))
        (hash-table-set! *cached-posts* id value))
    value))

(define (get-formatted-post id)
  (let ([post (load-post id)])
    (post-html-body-set! post (markdown->html
                               (post-body post)))
    post))

(define (main-page)
  (let* ([posts (map get-formatted-post
                     (recent-posts))]
         [id-list (get-last-id-per-day-alist
                   (map post-id posts))]
         [year-counts (map (lambda (year)
                             (cons year
                                   (length (posts-by-year year))))
                           (reverse (get-years)))])
    (sxml:main-page posts
                    (tag-list)
                    year-counts
                    (*contacts*)
                    (length (post-list)))))

(define (%month-page month year)
  (let* ([posts (map get-formatted-post
                     (posts-by-month month year))]
         [id-list (get-last-id-per-day-alist
                   (map post-id posts))])
    (sxml:month-page month year posts id-list)))

(define (month-page month year)
  (parameterize ([*url-prefix* "../../../"])
    (%month-page month year)))

(define (%year-page year)
  (let ([post-ids (posts-by-year year)]
        [months (get-months year)])
    (sxml:year-page year
                    months
                    (map load-post post-ids))))

(define (year-page year)
  (parameterize ([*url-prefix* "../../"])
    (%year-page year)))

(define (%archive-page)
  (sxml:archive-page (map (lambda (year)
                            (cons year (length (posts-by-year year))))
                          (get-years))))

(define (archive-page)
  (parameterize ([*url-prefix* "../"])
    (%archive-page)))

(define (%tag-page tag ids)
  (sxml:tag-page tag (map load-post (sort ids string-comparator))))

(define (tag-page tag)
  (parameterize ([*url-prefix* "../../"])
    (%tag-page tag (cdr (assoc tag (tag-list))))))

;; DIR must be relative!!
(define (copy-template-dir dir dst)
  (define (copy src)
    (let ([dst (build-path dst src)])
      (unless (file-is-directory? (sys-dirname dst))
        (make-directory* (sys-dirname dst)))
      (unless (and (file-exists? dst) (file-mtime>=? dst src))
        (copy-file src dst :if-exists :supersede))))
  (for-each copy (directory-fold dir cons '())))

(define (copy-template dst)
  (unless (file-is-directory? dst)
    (do-process (list "git" "init" dst)))
  (for-each (cut copy-template-dir <> dst)
            '("images" "smilies" "styles")))

(define (gen-pages :key (verbose #t))
  (define (get-url-and-proc x)
    (match x
      [('home) (list (url:home) main-page)]
      [('archives) (list (url:archives) archive-page)]
      [('year year) (list (url:year year)
                          (^[]  (year-page year)))]
      [('month month year) (list (url:month month year)
                                 (^[] (month-page month year)))]
      [('tag tag) (list (url:tag tag)
                        (^[] (tag-page tag)))]
      [else (raise (make-error x))]))

  (define (gen-page url proc)
    (call-with-output-file
        (build-path (*output-dir*) url)
      (lambda (port)
        (display "<!DOCTYPE html>\n" port)
        (display (sxml->html proc) port))))

  (define (gen-dir url)
    (receive (dir base ext)
        (decompose-path url)
      (make-directory* (build-path (*output-dir*) dir))))

  (let ([url-and-proc (map get-url-and-proc
                           (delete-duplicates *queued-pages*))])
    (for-each gen-dir (map car url-and-proc))
    (if verbose
        (for-each (lambda (x)
                    (display #"Generating ~(car x) ... ")
                    (apply gen-page x)
                    (print " done"))
                  url-and-proc)
        (let ([p (make-text-progress-bar :header "Generating"
                                         :max-value (length url-and-proc))])
          (for-each (lambda (x)
                      (apply gen-page x)
                      (p 'inc 1))
                    url-and-proc)
          (p 'finish)))
    (set! *queued-pages* '())))

(define (%queue-page! . item)
  (set! *queued-pages* (cons item *queued-pages*)))

(define (queue-main-page)
  #; (for-each collect-tags! (filter post-title
                                  (map load-post (post-list))))
  (%queue-page! 'home))

(define (queue-archive-page)
  (%queue-page! 'archives))

(define (queue-year-page year)
  (%queue-page! 'year year))

(define (queue-month-page month year)
  (%queue-page! 'month month year))

(define (queue-tag-page tag)
  (%queue-page! 'tag tag))

(define (queue-post-updates id)
  (queue-main-page)
  (queue-archive-page)
  (queue-year-page (post-id->year id))
  (queue-month-page (post-id->month id)
                    (post-id->year id)))

(define (queue-tag-pages)
  (for-each collect-tags! (filter post-title
                                  (map load-post (post-list))))
  (for-each queue-tag-page (map car (tag-list))))

(define (queue-all-pages)
  (define (queue-month-pages year)
    (for-each (lambda (month)
                (queue-month-page month year))
              (get-months year)))

  (define (queue-year-pages)
    (for-each (lambda (year)
                (queue-year-page year)
                (queue-month-pages year))
              (get-years)))

  (queue-tag-pages)
  (queue-main-page)
  (queue-archive-page)
  (queue-year-pages))

(define (queue-git-updates :key
                           (head "HEAD"))
  (queue-tag-pages)
  (for-each queue-post-updates
            (modified-posts :head head)))

(define (load-config path)
  (if (file-exists? path)
      (load path)))

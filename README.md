Cùi Bắp is a nanoblogger-inspired blogging system with blog posts
managed in a Git repository and written in Markdown.

Dependencies
---

- Gauche Scheme 0.9.6
- cmark 0.27.1
- git 2.11.0
- A UNIX-like environment (with `date` command at least)

Usage
---

Cùi Bắp at the moment only works from its working directory. No
installation or anything is needed.

```
git clone https://github.com/pclouds/cuibap.git
cd cuibap
./cb help
```

You'll need to specify the directory that contains blog posts. This
must be a Git repository. The default location is "data" under cuibap
directory. But you can customize it via `*git-dir*` parameter in
`config.scm`. For more customizations, see `cuibap/params.scm`.

```
cat >config.scm <<EOF
(use cuibap.params)
(use file.util)

(set! (*git-dir*) (expand-path ("~/blog")))
EOF
```

To prepare the initial output directory, do `./cb update-template`
which populates the output directory with static files. The default
output directory is "build", customisable in `*output-dir*`.

Now you can start adding a new blog post:

    ./cb new-post
    ./cb edit-post
    # satisfy? commit!
    ./cb commit-post
    # or not? delete the post
    ./cb cancel-post

`./cb src` and `./cb dst` are convenient wrappers that execute a
command on the blog repository or the output directory, e.g. if you
want to see diff or something. If no command is given, a new shell is
created on the given directory. `./cb both-src-dst` could be used to
issue the same command on both source and output directories
(e.g. "git diff" or "git push")

Normally when you commit a post, HTML files will be automatically
generated. If you want to manually regenerate something, there are the
commands

 - `update` that regenerates pages containing modified posts
 - `update-all` that regenerates the entire blog
 - `update-main` that only generates the main page

For very quick glance at the latest post, do

    ./cb update-main --max-entries=1

which generates the main page containing a single post.

To commit all changes in the output directory (which is also a Git
repository), do

    ./cb publish

Post format
---

A few special URLs are recognized in blog posts.

- URLs starting with `file:` are rewritten to access `files` directory
  in the blog repository.
- URLs starting with `lastfm:` are prefixed with
  `https://www.last.fm/`
- URLs that look like a post id (e.g. `YYYY-mm-ddTHH_MM_SS.txt`) are
  replaced with the post's permalink.